create table Institute (InstituteId bigint not null auto_increment, Address varchar(255), Email varchar(255) unique, Name varchar(255) not null, primary key (InstituteId)) engine=InnoDB;
create table Role (RoleId bigint not null auto_increment, RoleName varchar(255), primary key (RoleId)) engine=InnoDB;
create table User (UserId bigint not null auto_increment, Email varchar(255) unique, FirstName varchar(255), LastName varchar(255), Password varchar(255), UserName varchar(255) unique, InstituteId bigint, RoleId bigint not null, primary key (UserId)) engine=InnoDB;
--alter table Institute add constraint UK_aalpu5ars65bbddfsqmnp64ok unique (Email);
--alter table User add constraint UK_7xyi34i31xehayqd3heubave8 unique (Email);
alter table User add constraint FKoo4xuve44q3hhagd2ecpxn473 foreign key (InstituteId) references Institute (InstituteId);
alter table User add constraint FKgf02w9qexsiysosx4xhns6mic foreign key (RoleId) references Role (RoleId);
