package com.biofi.biofiserver.service.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import com.biofi.biofiserver.domain.Institute;
import com.biofi.biofiserver.domain.User;

public class UserDTO {

  private long id;

  @NotBlank
  @Size(min = 1, max = 50)
  private String userName;

  @Size(max = 50)
  private String firstName;

  @Size(max = 50)
  private String lastName;

  @Email
  @Size(min = 5, max = 100)
  private String email;

  // @OneToOne
  // @JoinColumn(name = "RoleId", nullable = false)
  // private Role role;

  // @OneToOne
  // @JoinColumn(name = "InstituteId")
  private Institute institute;

  public UserDTO() {}

  public UserDTO(User user) {
    this.id = user.getId();
    this.userName = user.getUserName();
    this.firstName = user.getFirstName();
    this.lastName = user.getLastName();
    this.email = user.getEmail();
    this.institute = user.getInstitute();
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Institute getInstitute() {
    return institute;
  }

  public void setInstitute(Institute institute) {
    this.institute = institute;
  }

}
