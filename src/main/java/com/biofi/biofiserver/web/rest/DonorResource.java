/**
 * 
 */
package com.biofi.biofiserver.web.rest;

import java.nio.file.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.biofi.biofiserver.storage.StorageService;

/**
 * @author joel
 *
 */
@RestController
@RequestMapping("/donors")
public class DonorResource {

  private final StorageService storageService;

  @Autowired
  public StringRedisTemplate stringTemplate;

  @Autowired
  public DonorResource(StorageService storageService) {
    this.storageService = storageService;
  }


  @PostMapping("/enroll")
  public String enroll(@RequestParam("file") MultipartFile file) {

    String filename = storageService.store(file);
    Path fsFile = storageService.load(filename);

    // This is redis example
    ValueOperations<String, String> ops = this.stringTemplate.opsForValue();
    String key = "spring.boot.redis.test";
    if (!this.stringTemplate.hasKey(key)) {
      ops.set(key, "foo");
    }
    System.out.println("Found key " + key + ", value=" + ops.get(key));

    // TODO: do something with uploaded file here. "Path" is the file object here.

    return "redirect:/";
  }

  @PostMapping("/match")
  public String match(@RequestParam("file") MultipartFile file) {

    String filename = storageService.store(file);
    Path fsFile = storageService.load(filename);

    // TODO: do something with uploaded file here. "Path" is the file object here.

    return "redirect:/";
  }
}
