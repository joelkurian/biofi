/**
 * 
 */
package com.biofi.biofiserver.web.rest;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.biofi.biofiserver.domain.User;
import com.biofi.biofiserver.service.UserService;

/**
 * @author joel
 *
 */
@RestController
@RequestMapping("/users")
@RolesAllowed("ROLE_ADMIN")
public class UserResource {

  @Autowired
  private UserService userService;

  @GetMapping
  public Collection<User> getAll() {
    return userService.getAll();
  }

  @GetMapping("/{id}")
  public Optional<User> getUser(@PathVariable Long id) {
    return userService.getUser(id);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteUser(@PathVariable Long id) {
    return userService.getUser(id).map(user -> {
      userService.deleteUser(id);
      return ResponseEntity.noContent().build();
    }).orElse(ResponseEntity.notFound().build());
  }

  @PostMapping
  public ResponseEntity<?> add(@ModelAttribute User user) {
    User newUser = userService.add(user);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(newUser.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @PutMapping("/{id}/password")
  @ResponseStatus(HttpStatus.OK)
  public void changePassword(@PathVariable Long id, @RequestParam("password") String password) {
    userService.setPassword(userService.getUser(id), password);
  }

}
