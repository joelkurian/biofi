/**
 * 
 */
package com.biofi.biofiserver.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.biofi.biofiserver.domain.User;
import com.biofi.biofiserver.service.UserService;

/**
 * @author joel
 *
 */
@Service
public class BiofiUserDetailsService implements UserDetailsService {

  @Autowired
  UserService userService;

  /*
   * (non-Javadoc)
   * 
   * @see
   * org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.
   * String)
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = userService.findUserByUserName(username).get();
    
    UserBuilder userBuilder = null;
    if (user != null) {
      userBuilder = org.springframework.security.core.userdetails.User.withUsername(username);
      userBuilder.password(user.getPassword());
      userBuilder.authorities("ROLE_" + user.getRole().getRoleName());
    }
    System.out.println(userBuilder.build());
    return userBuilder.build();
  }

}
