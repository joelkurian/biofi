package com.biofi.biofiserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.biofi.biofiserver.domain.Role;
import com.biofi.biofiserver.repository.RoleRepository;

@Service
public class RoleService {

  @Autowired
  private RoleRepository roleRepository;

  public Role add(Role role) {
    return roleRepository.save(role);
  }

  public List<Role> getAll() {
    Iterable<Role> roleIter = roleRepository.findAll();
    List<Role> roles = new ArrayList<>();
    roleIter.forEach(roles::add);
    return roles;
  }

  public Optional<Role> getRole(Long id) {
    return roleRepository.findById(id);
  }

  public void deleteRole(Long id) {
    roleRepository.deleteById(id);
  }



}
