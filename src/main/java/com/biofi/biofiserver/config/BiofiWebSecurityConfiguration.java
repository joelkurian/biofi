/**
 * 
 */
package com.biofi.biofiserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

/**
 * @author joel
 *
 */
//@Configuration
//@EnableWebSecurity
public class BiofiWebSecurityConfiguration extends WebSecurityConfigurerAdapter {

  @Autowired
  UserDetailsService userDetailsService;

  @Bean
  public static PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }

  @Override 
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
  }
  
  // @formatter:off
//  @Override
//  protected void configure(HttpSecurity http) throws Exception {
//    http
//    .authorizeRequests()
//        .antMatchers("/", "/home").permitAll()
//        .anyRequest().authenticated()
//        .and()
//    .formLogin()
//        .loginPage("/login")
//        .permitAll()
//        .and()
//    .logout()
//        .permitAll();
//    http.antMatcher("/**")
//      .authorizeRequests()
//        .antMatchers("/", "/login**", "/webjars/**", "/oauth/**").permitAll()
//        .anyRequest().authenticated();
//    .and().exceptionHandling()
//      .authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/")) 
    
//    http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and();
//    http.httpBasic().and();
//    http.authorizeRequests().anyRequest().hasAnyRole("ADMIN", "USER").and().csrf().disable();
    // http.httpBasic().and().authorizeRequests().anyRequest().authenticated().and().csrf().disable();

    // http.httpBasic();
    // http.authorizeRequests().anyRequest().authenticated();
    
//  }
  // @formatter:on

}
