/**
 * 
 */
package com.biofi.biofiserver.web.rest;

import java.security.Principal;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import com.biofi.biofiserver.service.UserService;
import com.biofi.biofiserver.service.dto.UserDTO;

/**
 * @author joel
 *
 */
@RestController
@RolesAllowed({"ROLE_USER", "ROLE_ADMIN"})
public class Account {

  @Autowired
  private UserService userService;

  @GetMapping("/user")
  public Optional<UserDTO> user(Principal user) {
    return userService.findUserByUserName(user.getName()).map(UserDTO::new);
  }

  @PostMapping("/change-password")
  @ResponseStatus(HttpStatus.OK)
  public void changePassword(@RequestParam(name = "currentPassword") String currentPassword,
      @RequestParam(name = "newPassword") String newPassword) {

  }
}
