package com.biofi.biofiserver.storage;

public class StorageFileNotFoundException extends StorageException {

  /**
  * 
  */
  private static final long serialVersionUID = 7814463751941367606L;

  public StorageFileNotFoundException(String message) {
    super(message);
  }

  public StorageFileNotFoundException(String message, Throwable cause) {
    super(message, cause);
  }
}
