--Dummy Institutes
INSERT INTO Institute (InstituteId, Address, Email, Name) VALUES(1, 'Ahmedabad', 'admin@admin.com', 'Admin Organization');
INSERT INTO Institute (InstituteId, Address, Email, Name) VALUES(2, 'Ahmedabad', 'support@who.com', 'WHO Blood Bank');
INSERT INTO Institute (InstituteId, Address, Email, Name) VALUES(3, 'Mumbai', 'contact@what.com', 'WHAT Hospital');

--Roles
INSERT INTO `Role` (RoleId, RoleName) VALUES(1, 'ADMIN');
INSERT INTO `Role` (RoleId, RoleName) VALUES(2, 'USER');

--Dummy Users
INSERT INTO `User` (UserId, Email, FirstName, LastName, Password, UserName, InstituteId, RoleId) VALUES(1, 'admin@admin.com', 'Admin', NULL, '{noop}admin123', 'admin', 1, 1);
INSERT INTO `User` (UserId, Email, FirstName, LastName, Password, UserName, InstituteId, RoleId) VALUES(2, 'johnsmith@test.com', 'John', 'Smith', '{noop}password', 'johnsmith', 2, 2);
INSERT INTO `User` (UserId, Email, FirstName, LastName, Password, UserName, InstituteId, RoleId) VALUES(3, 'janedoe@test1.com', 'Jane', 'Doe', '{noop}password', 'janedoe', 3, 2);
