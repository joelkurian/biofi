package com.biofi.biofiserver.repository;

import org.springframework.data.repository.CrudRepository;
import com.biofi.biofiserver.domain.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

}
