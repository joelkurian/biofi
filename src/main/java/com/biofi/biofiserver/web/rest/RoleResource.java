package com.biofi.biofiserver.web.rest;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.biofi.biofiserver.domain.Role;
import com.biofi.biofiserver.service.RoleService;

@RestController
@RequestMapping("/roles")
public class RoleResource {

  @Autowired
  private RoleService roleService;

  @PostMapping
  public ResponseEntity<?> add(@ModelAttribute Role role) {
    Role newRole = roleService.add(role);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(newRole.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @GetMapping
  public Collection<Role> getAll() {
    return roleService.getAll();
  }

  @GetMapping("/{id}")
  public Optional<Role> getRole(@PathVariable Long id) {
    return roleService.getRole(id);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity<?> deleteRole(@PathVariable Long id) {
    return roleService.getRole(id).map(role -> {
      roleService.deleteRole(id);
      return ResponseEntity.noContent().build();
    }).orElse(ResponseEntity.notFound().build());
  }
}
