/**
 * 
 */
package com.biofi.biofiserver.web.rest;

import java.net.URI;
import java.util.Collection;
import java.util.Optional;
import javax.annotation.security.RolesAllowed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.biofi.biofiserver.domain.Institute;
import com.biofi.biofiserver.service.InstituteService;

/**
 * @author joel
 *
 */
@RestController
@RequestMapping("/institutes")
@RolesAllowed("ROLE_ADMIN")
public class InstituteResource {

  @Autowired
  private InstituteService instituteService;

  @GetMapping
  public Collection<Institute> getAll() {
    return instituteService.getAll();
  }

  @GetMapping("/{id}")
  public Optional<Institute> getInstitute(@PathVariable Long id) {
    return instituteService.getInstitute(id);
  }

  @PostMapping
  public ResponseEntity<?> add(@ModelAttribute Institute institute) {
    Institute newInstitute = instituteService.add(institute);
    URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
        .buildAndExpand(newInstitute.getId()).toUri();
    return ResponseEntity.created(location).build();
  }

  @DeleteMapping("/{id}")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void deleteInstitute(@PathVariable Long id) {
    instituteService.deleteIntitute(id);
  }

}
