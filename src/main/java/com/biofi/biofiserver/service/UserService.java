/**
 * 
 */
package com.biofi.biofiserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import com.biofi.biofiserver.domain.User;
import com.biofi.biofiserver.repository.UserRepository;

/**
 * @author joel
 *
 */
@Component
public class UserService {

  @Autowired
  private UserRepository userRepository;

  @Bean
  public static PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }

  public List<User> getAll() {
    Iterable<User> userIter = userRepository.findAll();
    List<User> users = new ArrayList<>();
    userIter.forEach(users::add);
    return users;
  }

  public Optional<User> getUser(Long id) {
    return userRepository.findById(id);
  }

  public User add(User user) {
    user.setPassword(passwordEncoder().encode(user.getPassword()));
    return userRepository.save(user);
  }

  public void deleteUser(Long id) {
    userRepository.deleteById(id);
  }

  public Optional<User> findUserByUserName(String userName) {
    return userRepository.findByUserName(userName);
  }

  public User setPassword(Optional<User> oUser, String password) {
    User user = oUser.get();
    user.setPassword(passwordEncoder().encode(password));
    return userRepository.save(user);
  }
}
