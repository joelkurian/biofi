package com.biofi.biofiserver.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Transient;

@Entity
@Table(name = "User")
public class User implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = -1583809713126355783L;

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "UserId")
  private long id;

  @Column(name = "UserName")
  private String userName;

  @Column(name = "FirstName")
  private String firstName;

  @Column(name = "LastName")
  private String lastName;

  @Column(name = "Email", unique = true)
  @Email(message = "*Please provide a valid Email")
  @NotEmpty(message = "*Please provide an email")
  private String email;

  @Column(name = "Password")
  @Length(min = 8, message = "*Your password must have at least 8 characters")
  @NotEmpty(message = "*Please provide your password")
  @Transient
  private String password;

  @OneToOne
  @JoinColumn(name = "RoleId", nullable = false)
  private Role role;

  @OneToOne
  @JoinColumn(name = "InstituteId")
  private Institute institute;

  public User() {}

  public User(String userName, String firstName, String lastName, String email, String password,
      Role role, Institute institute) {
    super();
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.password = password;
    this.role = role;
    this.institute = institute;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public Institute getInstitute() {
    return institute;
  }

  public void setInstitute(Institute institute) {
    this.institute = institute;
  }

  @Override
  public String toString() {
    return "User [id=" + id + ", userName=" + userName + ", firstName=" + firstName + ", lastName="
        + lastName + ", email=" + email + ", role=" + role + ", institute=" + institute + "]";
  }

}
