/**
 * 
 */
package com.biofi.biofiserver.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.biofi.biofiserver.domain.Institute;
import com.biofi.biofiserver.repository.InstituteRepository;

/**
 * @author joel
 *
 */
@Component
public class InstituteService {

  @Autowired
  private InstituteRepository instituteRepository;

  public List<Institute> getAll() {
    Iterable<Institute> instituteIter = instituteRepository.findAll();
    List<Institute> institutes = new ArrayList<>();
    instituteIter.forEach(institutes::add);
    return institutes;
  }

  public Optional<Institute> getInstitute(Long id) {
    return instituteRepository.findById(id);
  }

  public Institute add(Institute institute) {
    return instituteRepository.save(institute);
  }

  public void deleteIntitute(Long id) {
    instituteRepository.deleteById(id);
  }

}
