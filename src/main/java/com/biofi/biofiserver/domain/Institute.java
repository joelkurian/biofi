/**
 * 
 */
package com.biofi.biofiserver.domain;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author joel
 *
 */
@Entity
@Table(name = "Institute")
public class Institute implements Serializable {
  /**
   * 
   */
  private static final long serialVersionUID = 6538237794708908954L;
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "InstituteId")
  private long id;

  @Column(name = "Name")
  private String name;

  @NotNull
  @Column(name = "Email", unique = true)
  private String email;

  @Column(name = "Address")
  private String address;

  public Institute() {}

  public Institute(long id, String name, String email, String address) {
    super();
    this.id = id;
    this.name = name;
    this.address = address;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public static long getSerialversionuid() {
    return serialVersionUID;
  }

  @Override
  public String toString() {
    return "Institute [id=" + id + ", name=" + name + ", email=" + email + ", address=" + address
        + "]";
  }

}
