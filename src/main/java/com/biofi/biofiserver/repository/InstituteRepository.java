package com.biofi.biofiserver.repository;

import org.springframework.data.repository.CrudRepository;
import com.biofi.biofiserver.domain.Institute;

public interface InstituteRepository extends CrudRepository<Institute, Long> {

}
