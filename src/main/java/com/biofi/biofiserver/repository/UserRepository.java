/**
 * 
 */
package com.biofi.biofiserver.repository;

import java.util.Optional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.biofi.biofiserver.domain.User;

/**
 * @author joel
 *
 */
@Repository
public interface UserRepository extends CrudRepository<User, Long> {

  Optional<User> findByUserName(String userName);

}
